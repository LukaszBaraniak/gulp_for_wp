jQuery(document).ready(function ( $ ) {
  $('a[href^="#"]').on('click', function(event) {
    var target = $( $(this).attr('href') );
      if( target.length ) {
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 2000);
      }
    });
  // Get page title
  var pageTitle = $("title").text();

	// Change page title on blur
	$(window).blur(function() {
	  $("title").text("Wróć do nas!");
	});

	// Change page title back on focus
	$(window).focus(function() {
	  $("title").text(pageTitle);
	}); 
  

});